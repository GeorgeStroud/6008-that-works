﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App3
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailsPage : ContentPage
    {
        public DetailsPage(string URL, string DESC, string Title, string Number, string Release)
        {
            InitializeComponent();

            Poster.Source = URL;
            Decipt.Text = DESC;
            Name.Text = Title;
            Rating.Text = Number;
                Dor.Text = Release;


        }
    }
}
