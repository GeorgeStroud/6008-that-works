﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
namespace App3
{
    public partial class MainPage : ContentPage
    {
        List<Movie> AllInformation = new List<Movie>
        {   new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTY3NjY0MTQ0Nl5BMl5BanBnXkFtZTcwMzQ2MTc0Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "When a criminal mastermind uses a trio of orphan girls as pawns for a grand scheme, he finds their love is profoundly changing him for the better.	", Name = "Despicable Me", Rating = "7.7/10", Dor="15 Oct 2010" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BOGZhM2FhNTItODAzNi00YjA0LWEyN2UtNjJlYWQzYzU1MDg5L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "After his swamp is filled with magical creatures, Shrek agrees to rescue Princess Fiona for a villainous lord in order to get his land back.", Name = "Shrek", Rating = "7.9/10", Dor="29 June 2001"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BMDliMmNhNDEtODUyOS00MjNlLTgxODEtN2U3NzIxMGVkZTA1L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "Russel Crowe kills stuff in Rome as a Gladiator", Name = "Gladiator", Rating = "8.5/10", Dor="12 May 2000" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BYThjM2MwZGMtMzg3Ny00NGRkLWE4M2EtYTBiNWMzOTY0YTI4XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_UY1200_CR139,0,630,1200_AL_.jpg", Descipt = "While not intelligent, Forrest Gump has accidentally been present at many historic moments, but his true love, Jenny Curran, eludes him.", Name = "Forrest Gump", Rating = "8.8/10", Dor="7 October 1994"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTk3Mjk5MzI3OF5BMl5BanBnXkFtZTcwMTY4MzcyNA@@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "Two seemingly dumb teens struggle to prepare a historical presentation with the help of a time machine.", Name = "Bill and Teds Excellent Adventure", Rating = "6.9/10", Dor = "25 August 1989"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "Three decades after the defeat of the Galactic Empire, a new threat arises. The First Order attempts to rule the galaxy and only a ragtag group of heroes can stop them, along with the help of the Resistance.", Name = "Star Wars the Force Awakens", Rating = "8.1/10", Dor = "17 December 2015" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BMGU2NzRmZjUtOGUxYS00ZjdjLWEwZWItY2NlM2JhNjkxNTFmXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "A cyborg, identical to the one who failed to kill Sarah Connor, must now protect her teenage son, John Connor, from a more advanced cyborg.", Name = "Terminator 2 Judgement Day", Rating = "8.5/10", Dor = "16 August 1991" },
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BODkyNDQzMzUzOF5BMl5BanBnXkFtZTcwODYyMDEyOA@@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "In a violent, futuristic city where the police have the authority to act as judge, jury and executioner, a cop teams with a trainee to take down a gang that deals the reality-altering drug, SLO-MO.", Name = "Dredd", Rating = "7.1/10", Dor = "7 September 2012"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BYmM1NzUyY2YtOTU4My00ZmFmLWI4ZjYtYzJlM2JlOTBhZmIzXkEyXkFqcGdeQXVyNjE5MjUyOTM@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "In a dystopian future, Joseph Dredd, the most famous Judge (a police officer with instant field judiciary powers), is convicted for a crime he did not commit and must face his murderous counterpart.", Name = "Judge Dredd", Rating = "5.5/10", Dor = "21 July 1995	"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BZDdkMmI5NWYtOGNiNi00ZjZmLThiYjItZGRiMTAwMWYxYjJhXkEyXkFqcGdeQXVyNDc2NjEyMw@@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "A police officer is brought out of suspended animation in prison to pursue an old ultra-violent nemesis who is loose in a non-violent future society.", Name = "Demolition Man", Rating = "6.6/10", Dor = "12 November 1993"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BODAzMDgxMDc1MF5BMl5BanBnXkFtZTgwMTI0OTAzMjE@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness.", Name = "The Hobbit The Battle of the Five Armies", Rating = "7.4/10", Dor = "12 December 2014"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BYjA0NDMyYTgtMDgxOC00NGE0LWJkOTQtNDRjMjEzZmU0ZTQ3XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "A gunslinger is embroiled in a war with a local drug runner.", Name = "Desperado", Rating = "7.2/10", Dor = "9 February 1996 "},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BNjI3NzI2MjY2MF5BMl5BanBnXkFtZTYwMDM5ODU5._V1_UY268_CR6,0,182,268_AL_.jpg", Descipt = "A young thief, seeking revenge for the death of his brother, is trained by the once great, but aged Zorro, who also pursues vengeance of his own.", Name = "The Mask of Zorro", Rating = "6.7/10", Dor = "11 December 1998"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BNzMxNTExOTkyMF5BMl5BanBnXkFtZTcwMzEyNDc0OA@@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "A video game villain wants to be a hero and sets out to fulfill his dream, but his quest brings havoc to the whole arcade where he lives.", Name = "Wreck-It Ralph", Rating = "7.8/10", Dor = "8 Febuary 2013"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BMDliOTIzNmUtOTllOC00NDU3LWFiNjYtMGM0NDc1YTMxNjYxXkEyXkFqcGdeQXVyNTM3NzExMDQ@._V1_UY268_CR3,0,182,268_AL_.jpg", Descipt = "The special bond that develops between plus-sized inflatable robot Baymax, and prodigy Hiro Hamada, who team up with a group of friends to form a band of high-tech heroes.", Name = "Big Hero 6", Rating = "7.8/10", Dor = "30 January 2015"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BM2FmZTk5NDUtMzU0Yy00NzgzLWI0Y2MtMjExOWJmMmFlNzk1L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "An American serving in the French Foreign Legion on an archaeological dig at the ancient city of Hamunaptra accidentally awakens a mummy.", Name = "The Mummy", Rating = "7.0/10", Dor = "25 June 1999"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BODRmY2NhNDItOWViNi00OTIyLTk3YjYtYzY0YTFlMDg1YzQ0L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UY268_CR1,0,182,268_AL_.jpg", Descipt = "The notorious monster hunter is sent to Transylvania to stop Count Dracula who is using Dr. Frankenstein's research and a werewolf for some sinister purpose.", Name = "Van Helsing", Rating = "6/10", Dor = "7 May 2004"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BYWQ5ZDM3NjUtMzJhNy00MTFmLThiY2MtNjNhZDdlODE4M2RiXkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_UY268_CR0,0,182,268_AL_.jpg", Descipt = "Their women having been enslaved by the local pack of lesbian vampires thanks to an ancient curse, the remaining menfolk of a rural town send two hapless young lads out onto the moors as a sacrifice.", Name = "Lesbian Vampire Killers", Rating = "5.2/10", Dor = "10 March 2009"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BYWM2NDZmYmYtNzlmZC00M2MyLWJmOGUtMjhiYmQ2OGU1YTE1L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "A loyal and dedicated Hong Kong inspector teams up with a reckless and loud mouthed LAPD detective to rescue the Chinese Consul's kidnapped daughter, while trying to arrest a dangerous crime lord along the way.", Name = "Rush Hour", Rating = "6.9/10", Dor = "4 December 1998"},
            new Movie { Url = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTY3MTI5NjQ4Nl5BMl5BanBnXkFtZTcwOTU1OTU0OQ@@._V1_UX182_CR0,0,182,268_AL_.jpg", Descipt = "As a war between humankind and monstrous sea creatures wages on, a former pilot and a trainee are paired up to drive a seemingly obsolete special weapon in a desperate effort to save the world from the apocalypse.", Name = "Pacific Rim", Rating = "7/10", Dor = "12 July 2013"},
        };



        public MainPage()
        {
            InitializeComponent();

            

            Image1.Source = AllInformation[0].Url;
            Image2.Source = AllInformation[1].Url;
            Image3.Source = AllInformation[2].Url;
            Image4.Source = AllInformation[3].Url;
            Image5.Source = AllInformation[4].Url;
            Image6.Source = AllInformation[5].Url;
            Image7.Source = AllInformation[6].Url;
            Image8.Source = AllInformation[7].Url;
            Image9.Source = AllInformation[8].Url;
            Image10.Source = AllInformation[9].Url;
            Image11.Source = AllInformation[10].Url;
            Image12.Source = AllInformation[11].Url;
            Image13.Source = AllInformation[12].Url;
            Image14.Source = AllInformation[13].Url;
            Image15.Source = AllInformation[14].Url;
            Image16.Source = AllInformation[15].Url;
            Image17.Source = AllInformation[16].Url;
            Image18.Source = AllInformation[17].Url;
            Image19.Source = AllInformation[18].Url;
            Image20.Source = AllInformation[19].Url;

            var tap = new TapGestureRecognizer();
            tap.Tapped += Tapp;

            Image1.GestureRecognizers.Add(tap);
            Image2.GestureRecognizers.Add(tap);
            Image3.GestureRecognizers.Add(tap);
            Image4.GestureRecognizers.Add(tap);
            Image5.GestureRecognizers.Add(tap);
            Image6.GestureRecognizers.Add(tap);
            Image7.GestureRecognizers.Add(tap);
            Image8.GestureRecognizers.Add(tap);
            Image9.GestureRecognizers.Add(tap);
            Image10.GestureRecognizers.Add(tap);
            Image11.GestureRecognizers.Add(tap);
            Image12.GestureRecognizers.Add(tap);
            Image13.GestureRecognizers.Add(tap);
            Image14.GestureRecognizers.Add(tap);
            Image15.GestureRecognizers.Add(tap);
            Image16.GestureRecognizers.Add(tap);
            Image17.GestureRecognizers.Add(tap);
            Image18.GestureRecognizers.Add(tap);
            Image19.GestureRecognizers.Add(tap);
            Image20.GestureRecognizers.Add(tap);


             void Tapp(object sender, EventArgs e)
            {

                var a = (int)((BindableObject)sender).GetValue(Grid.RowProperty) * 4;

                var b = (int)((BindableObject)sender).GetValue(Grid.ColumnProperty);



                Navigation.PushAsync(new DetailsPage(AllInformation[a + b].Url, AllInformation[a + b].Descipt, AllInformation[a + b].Name, AllInformation[a + b].Rating, AllInformation[a + b].Dor));



            }


        }
    } }










